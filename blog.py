import os
import webapp2
import jinja2
import json
import datetime
import re
import random
import string
import hashlib
import hmac
import logging
import time

from google.appengine.api import memcache
from google.appengine.ext import db

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape=False)

#################
####Utilities####
#################

html_escape_table = {
    "&": "&amp;",
    '"': "&quot;",
    "'": "&apos;",
    ">": "&gt;",
    "<": "&lt;",
    }

USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
PASSWORD_RE = re.compile(r"^.{3,20}$")
EMAIL_RE = re.compile(r"^[\S]+@[\S]+\.[\S]+$")

def escape_html(s):
    """Produce entities within text."""
    line = ""
    for c in s:
        line = line+ html_escape_table.get(c,c)

    return line.replace("\n", "<br>")


# Signup form verifications

def valid_username(username):
	return USER_RE.match(username)

def is_existing(username):
	existing_names = db.GqlQuery("SELECT name FROM User")
	return username in existing_names

def valid_password(password):
	return PASSWORD_RE.match(password)


def valid_verify(password, verify):
	if password == verify:
		return True

def valid_email(email=""):
	if email:
		return EMAIL_RE.match(email)
	else:
		return True

# User hashing

def make_salt():
    	return ''.join(random.choice(string.letters) for x in xrange(5))
   
def make_userhash(name, pw, salt=None):
	"""used to hash password before making new User entity	pashed_pw|salt"""
    	if not salt:
        	salt = make_salt()
    	h = hmac.new(salt, str(name)+str(pw)).hexdigest()
    	return '%s|%s' % (h, salt)

def make_user_cookie(user_id):
	"""used in header request to set cookie. user_id|hashed_pw"""
	user = User.get_by_id(int(user_id))
	pw_hash = user.pw_hash.split('|')[0]
	return "%s|%s" % (str(user_id), pw_hash)

def verify_user_cookie(user_cookie):
	"""at Welcome, checks if user_id cookie has id == id and hash == hash """
	num = user_cookie.split('|')[0]
	pw_hash= user_cookie.split('|')[1]
	user = User.get_by_id(int(num))
	if user:
		return user.pw_hash.split('|')[0] == pw_hash

def user_from_cookie(user_cookie_str):		# at Welcome, given user_id cookie returns corresponding User entity
	"""at Welcome, given user_id cookie returns corresponding User entity"""
	user_id = int(user_cookie_str.split("|")[0])
	return User.get_by_id(int(user_id))

def verify_login(name, pw):								# used at login
	""" Login verification, checks for existing User with matching hash """
	userdata = db.GqlQuery("SELECT * FROM User WHERE name = :1",name).get()
	if userdata:
		usersalt = str(userdata.pw_hash.split('|')[1])
		input_hash = make_userhash(name, pw, usersalt)
		if input_hash == userdata.pw_hash:
			return True

    
    
#####################
#### Blog Handler####
#####################

class Handler(webapp2.RequestHandler):
	def write(self, *a, **kw):
      		self.response.out.write(*a, **kw)

    	def render_str(self, template, **params):
        	t = jinja_env.get_template(template)
        	return t.render(params)

    	def render(self, template, **kw):
        	self.write(self.render_str(template, **kw))

	def render_json(self, pdict):
		json_front = json.dumps(pdict)
		self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
         	self.response.out.write(json_front)
		
	def initialize(self, *a, **kw):
		webapp2.RequestHandler.initialize(self, *a, **kw)
		if self.request.url.endswith('.json'):
            		self.format = 'json'
        	else:
            		self.format = 'html'

class Post(db.Model):
    subject = db.StringProperty(required = True)
    content = db.TextProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)
    modified = db.DateTimeProperty(auto_now = True)

    def as_dict(self):
	    dictionary = {"content": self.content,
			  "created": self.created.strftime("%a %b %d %H:%M:%S %Y"),
			  "modified": self.modified.strftime("%a %b %d %H:%M:%S %Y"),
			  "subject": self.subject}
	    return dictionary

class User(db.Model):
	name = db.StringProperty(required = True)
	pw_hash = db.StringProperty(required = False)
	email = db.StringProperty(required = False)


####################
#### Blog Pages ####
####################

class BlogFront(Handler):
	def query_time(self):
		q_time = time.time() - memcache.get('top')[1]
		#logging.info("QUERY TIME IS %d" % q_time)
		return int(q_time)

	def get(self):
		posts = display_posts()
		age = self.query_time()
		if self.format == 'html':
			self.render("blogfront.html", posts= posts, age=age)
		else:
			return self.render_json([p.as_dict() for p in posts])

class NewPost(Handler):
	def render_newpost(self, subject="", content="", error=""):
		self.render("newpost.html", subject=subject, content=content, error=error)

	def get(self):
		self.render_newpost()

	def post(self):
		subject = self.request.get("subject")
		content = self.request.get("content")

		if subject and content:
			content = escape_html(content)
			p = Post(subject=subject, content=content)
			p.put()

			self.redirect("/blog/"+ str(p.key().id()))

		else:
			error = "we need both a subject and content!"
			self.render_newpost(subject, content, error)

class PostPermalink(Handler):
	def query_time(self, post_id):
		q_time = time.time() - memcache.get(post_id)[1]
		logging.info("POST QUERY TIME IS %d" % q_time)
		return int(q_time)

	def get(self, post_id):
		entry = display_posts(post_id, update = True)
		age = self.query_time(str(post_id))
		if entry:
			if self.format == "html":
				self.render("post.html", subject=entry.subject, content=entry.content, date=entry.created, age=age)
			else:
				return self.render_json(entry.as_dict())

class FlushCache(Handler):
	def get(self):
		memcache.flush_all()
		self.redirect("/blog")

#####################
#### User Signup ####
#####################

class SignupPage(Handler):
	def render_signup(self, username="", email="", ue="", pwe="", ve="", eer=""):
		self.render("signup.html", username = username, email = email, username_error=ue, password_error = pwe, verify_error = ve, email_error = eer)

	def get(self):
		self.render_signup()

	def post(self):
        	username_error = ""
		password_error=""
		verify_error=""
		email_error=""
		user_username = self.request.get('username')
		user_password = self.request.get('password')
		user_verify = self.request.get('verify')
		user_email = self.request.get('email')

		check = True

		if valid_username(user_username):
			existing = User.gql("WHERE name= :1", user_username).get()	# check if username is already in datastore
			if existing:
				username_error = "That user already exists yo."
				check = False
		else:
			username_error = "That's not a valid username."
			check = False

		if not valid_password(user_password):
			password_error = "That wasn't a valid password."
			check = False
		if not valid_verify(user_password, user_verify):
			verify_error = "Your passwords didn't match."
			check = False
		if not valid_email(user_email):
			email_error = "That's not a valid email."
			check = False
		if is_existing(user_username):
			check = False
	
		if check:
			pw_hash = make_userhash(user_username, user_password)
			new_user = User(name = user_username, pw_hash = pw_hash, email = user_email)
			new_user.put()

			self.response.headers.add_header('set-cookie', 'user_id=%s; Path=/' % str(make_user_cookie(new_user.key().id())))
			
			self.redirect("/blog/welcome")
		else:
			self.render_signup(user_username, user_email, username_error, password_error, verify_error, email_error)

class LoginPage(Handler):
	def render_login(self, username="", error=""):
		self.render("login.html", username = username, login_error = error)

	def get(self):
		self.render_login()

	def post(self):
		login_error = ""
		user_username = self.request.get('username')
		user_password = self.request.get('password')
		
		if verify_login(str(user_username), str(user_password)):
			q = db.GqlQuery("SELECT * FROM User WHERE name = :1", user_username).get()

			self.response.headers.add_header('set-cookie', 'user_id=%s; Path=/' % str(make_user_cookie(q.key().id())))
			self.redirect("/blog/welcome")
		else:
			login_error = "Invalid user information."
			self.render_login(user_username, login_error)

class LogoutPage(Handler):
	def get(self):
		self.response.headers.add_header('Set-Cookie', 'user_id=; Path=/')
		self.redirect("/blog/signup")

class WelcomePage(Handler):
	def get(self):
		user_cookie = self.request.cookies.get('user_id')
		if user_cookie:
			if verify_user_cookie(user_cookie):
				user = user_from_cookie(user_cookie)
				self.response.out.write("Welcome, %s!" % user.name)
		else:
			self.redirect('/blog/signup')

###################
### CACHE TIMES ###
###################

def display_posts(post_id = None, update = False):
	# Caching for single post
	if post_id:
		post = memcache.get(str(post_id))
		if post is None:
			post = Post.get_by_id(int(post_id))
			query_post = time.time()
			memcache.set(str(post_id), (post, query_post))
		else:
			post = post[0]
		
	# Caching for front page
	posts = memcache.get('top')	
	if posts is None or update:
		logging.info("DB QUERY")
		query_front = time.time()
		posts = db.GqlQuery("SELECT * FROM Post ORDER BY created DESC LIMIT 10")
		posts = list(posts)
		memcache.set('top', (posts, query_front))
	else:
		posts = posts[0]
	
	if post_id:
		return post
	else:
		return posts


app = webapp2.WSGIApplication([(r'/blog/?(?:.json)?', BlogFront),
	('/blog/newpost/?', NewPost),
	(r'/blog/([0-9]+)(?:.json)?', PostPermalink),
	('/blog/signup/?', SignupPage),
	('/blog/login/?', LoginPage),
	('/blog/logout/?', LogoutPage),
	('/blog/welcome/?', WelcomePage),
	('/blog/flush/?', FlushCache)], debug=True)



